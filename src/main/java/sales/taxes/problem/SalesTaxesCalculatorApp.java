package sales.taxes.problem;

import sales.taxes.problem.reader.PlainTextSalesTaxesReader;

public class SalesTaxesCalculatorApp {

  public String demoOfSalesTaxesCalculator(String shoppingBasketFilename) {
    SalesTaxesCalculator salesTaxesCalculator = new SalesTaxesCalculator();
    salesTaxesCalculator.setReader(new PlainTextSalesTaxesReader());
    salesTaxesCalculator.fetchKnowledgeOfTaxFreeGoods("tax_free_items.txt");
    salesTaxesCalculator.fetchShoppingBasket(shoppingBasketFilename);
    String receipt = salesTaxesCalculator.getReceipt();
    System.out.println(receipt);
    System.out.println();
    return receipt;
  }

  public static void main(String[] args) {

    SalesTaxesCalculatorApp app = new SalesTaxesCalculatorApp();

    System.out.println("Output 1:");
    app.demoOfSalesTaxesCalculator("input/first_shopping_basket.txt");

    System.out.println("Output 2:");
    app.demoOfSalesTaxesCalculator("input/second_shopping_basket.txt");

    System.out.println("Output 3:");
    app.demoOfSalesTaxesCalculator("input/third_shopping_basket.txt");
  }
}
