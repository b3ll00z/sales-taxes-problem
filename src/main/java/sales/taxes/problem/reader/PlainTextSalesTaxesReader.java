package sales.taxes.problem.reader;

import java.io.File;
import java.net.URL;
import java.util.*;

public class PlainTextSalesTaxesReader implements SalesTaxesReader {
  @Override
  public List<String> downloadResource(String filename) {
    List<String> taxFreeItems = new ArrayList<>();

    try {
      URL taxFreeItemsResource = getClass().getClassLoader().getResource(filename);
      File taxFreeItemsChannel = new File(Objects.requireNonNull(taxFreeItemsResource).getFile());
      Scanner untaxedItemReader = new Scanner(taxFreeItemsChannel);

      while (untaxedItemReader.hasNextLine()) {
        taxFreeItems.add(untaxedItemReader.nextLine());
      }

      return taxFreeItems;
    } catch (Exception e) {
      throw new IllegalArgumentException("Resource file not found!");
    }
  }
}
