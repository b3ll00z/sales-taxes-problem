package sales.taxes.problem.reader;

import java.util.List;

public interface SalesTaxesReader {
  List<String> downloadResource(String filename);
}
