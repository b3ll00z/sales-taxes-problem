package sales.taxes.problem.item;

import java.math.BigDecimal;

public class Item {

  private static final BigDecimal ONE_HUNDRED = new BigDecimal("100");
  private static final BigDecimal ROUNDING_RATE = new BigDecimal("0.05");
  private static final String IMPORTED = "imported ";
  private BigDecimal price;
  private BigDecimal quantity;
  private BigDecimal totalSaleTax;
  private String description;
  private String name;

  public Item(String itemDescription) {
    description = itemDescription;
    totalSaleTax = new BigDecimal("0.00");
    extractPriceAndSalesTax();
  }

  private void extractPriceAndSalesTax() {
    int positionOfCost = description.lastIndexOf("at") + 3;
    price = new BigDecimal(description.substring(positionOfCost));

    String quantityAndItemName = description.substring(0, positionOfCost - 3).trim();
    String[] itemAttributes = quantityAndItemName.split(" ");
    quantity = new BigDecimal(itemAttributes[0]);

    int positionOfQuantity = description.indexOf(itemAttributes[0]);
    name = description.substring(positionOfQuantity + 1, positionOfCost - 3).trim();

    if (name.contains(IMPORTED)) {
      name = name.replace(IMPORTED, "").trim();
    }
  }

  public BigDecimal getTotalPrice() {
    return price.add(totalSaleTax).multiply(quantity);
  }

  public BigDecimal getSalesTax() {
    return totalSaleTax.multiply(quantity);
  }

  private void applyTax(String taxNumerator) {
    BigDecimal costTaxed = price.multiply(new BigDecimal(taxNumerator)).divide(ONE_HUNDRED, 0);
    totalSaleTax =
        totalSaleTax.add(
            costTaxed.divide(ROUNDING_RATE, 0, BigDecimal.ROUND_UP).multiply(ROUNDING_RATE));
  }

  public void applyBasicSaleTax() {
    applyTax("10");
  }

  public void applyImportDuty() {
    applyTax("5");
  }

  public String getName() {
    return name;
  }

  public String getResume() {
    StringBuilder resume = new StringBuilder();
    resume.append(quantity.toPlainString()).append(" ");

    if (description.contains(IMPORTED)) {
      resume.append(IMPORTED);
    }

    resume.append(name).append(": ").append(getTotalPrice().toPlainString());
    return resume.toString();
  }
}
