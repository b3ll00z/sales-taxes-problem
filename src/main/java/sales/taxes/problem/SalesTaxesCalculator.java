package sales.taxes.problem;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import sales.taxes.problem.item.Item;
import sales.taxes.problem.reader.SalesTaxesReader;

public class SalesTaxesCalculator {
  private List<Item> basket;
  private SalesTaxesReader salesTaxesReader;
  private TreeSet<String> taxFreeItems;

  public SalesTaxesCalculator() {
    basket = new ArrayList<>();
    taxFreeItems = new TreeSet<>();
  }

  public void setReader(SalesTaxesReader reader) {
    salesTaxesReader = reader;
  }

  public void fetchKnowledgeOfTaxFreeGoods(String untaxedGoodsKnowledgeFilename) {
    taxFreeItems.addAll(salesTaxesReader.downloadResource(untaxedGoodsKnowledgeFilename));
  }

  public void fetchShoppingBasket(String shoppingBasketFilename) {
    List<String> shoppingBasket = salesTaxesReader.downloadResource(shoppingBasketFilename);

    for (String detailsOfOrderedItem : shoppingBasket) {
      Item item = new Item(detailsOfOrderedItem);

      if (detailsOfOrderedItem.contains("imported")) {
        item.applyImportDuty();
      }

      if (!taxFreeItems.contains(item.getName())) {
        item.applyBasicSaleTax();
      }

      basket.add(item);
    }
  }

  public String getTotalPrice() {
    Stream<BigDecimal> itemsPrices = basket.stream().map(Item::getTotalPrice);
    BigDecimal totalImport = itemsPrices.reduce(BigDecimal.ZERO, BigDecimal::add);
    return totalImport.toPlainString();
  }

  public String getSalesTaxes() {
    Stream<BigDecimal> itemsSalesTaxes = basket.stream().map(Item::getSalesTax);
    BigDecimal totalSalesTaxes = itemsSalesTaxes.reduce(BigDecimal.ZERO, BigDecimal::add);
    return totalSalesTaxes.toPlainString();
  }

  public String getReceipt() {
    StringBuilder receipt = new StringBuilder();
    String allItemsResumes = basket.stream().map(Item::getResume).collect(Collectors.joining("\n"));
    receipt.append(allItemsResumes);
    receipt.append("\nSales Taxes: ").append(getSalesTaxes()).append("\n");
    receipt.append("Total: ").append(getTotalPrice());
    return receipt.toString();
  }
}
