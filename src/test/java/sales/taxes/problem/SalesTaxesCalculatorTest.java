package sales.taxes.problem;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import sales.taxes.problem.reader.PlainTextSalesTaxesReader;

public class SalesTaxesCalculatorTest {

  private SalesTaxesCalculator salesTaxesCalculator;

  @BeforeEach
  public void setUp() {
    salesTaxesCalculator = new SalesTaxesCalculator();
    salesTaxesCalculator.setReader(new PlainTextSalesTaxesReader());
  }

  @Test
  public void illegalArgumentOnTaxFreeItemsKnowledgeNotFound() {
    try {
      salesTaxesCalculator.fetchKnowledgeOfTaxFreeGoods("nowhere_file.txt");
      failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
    } catch (IllegalArgumentException e) {
      assertThat(e).hasMessage("Resource file not found!");
    }
  }

  @Test
  public void illegalArgumentOnShoppingBasketNotFound() {
    try {
      salesTaxesCalculator.fetchShoppingBasket("nowhere_file.txt");
      failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
    } catch (IllegalArgumentException e) {
      assertThat(e).hasMessage("Resource file not found!");
    }
  }

  @Test
  public void resumeOnFirstShoppingBasket() {
    salesTaxesCalculator.fetchKnowledgeOfTaxFreeGoods("tax_free_items.txt");
    salesTaxesCalculator.fetchShoppingBasket("input/first_shopping_basket.txt");
    assertThat(salesTaxesCalculator.getTotalPrice()).isEqualTo("42.32");
    assertThat(salesTaxesCalculator.getSalesTaxes()).isEqualTo("1.50");
  }

  @Test
  public void resumeOnSecondShoppingBasket() {
    salesTaxesCalculator.fetchKnowledgeOfTaxFreeGoods("tax_free_items.txt");
    salesTaxesCalculator.fetchShoppingBasket("input/second_shopping_basket.txt");
    assertThat(salesTaxesCalculator.getTotalPrice()).isEqualTo("65.15");
    assertThat(salesTaxesCalculator.getSalesTaxes()).isEqualTo("7.65");
  }

  @Test
  public void resumeOnThirdShoppingBasket() {
    salesTaxesCalculator.fetchKnowledgeOfTaxFreeGoods("tax_free_items.txt");
    salesTaxesCalculator.fetchShoppingBasket("input/third_shopping_basket.txt");
    assertThat(salesTaxesCalculator.getTotalPrice()).isEqualTo("98.38");
    assertThat(salesTaxesCalculator.getSalesTaxes()).isEqualTo("7.90");
  }

  @Test
  public void receiptOnFirstShoppingBasket() throws Exception {
    salesTaxesCalculator.fetchKnowledgeOfTaxFreeGoods("tax_free_items.txt");
    salesTaxesCalculator.fetchShoppingBasket("input/first_shopping_basket.txt");
    String expectedReceipt =
        getExpectedReceipt("output/expected_first_shopping_basket_receipt.txt");
    assertThat(salesTaxesCalculator.getReceipt()).isEqualTo(expectedReceipt);
  }

  @Test
  public void receiptOnSecondShoppingBasket() throws Exception {
    salesTaxesCalculator.fetchKnowledgeOfTaxFreeGoods("tax_free_items.txt");
    salesTaxesCalculator.fetchShoppingBasket("input/second_shopping_basket.txt");
    String expectedReceipt =
        getExpectedReceipt("output/expected_second_shopping_basket_receipt.txt");
    assertThat(salesTaxesCalculator.getReceipt()).isEqualTo(expectedReceipt);
  }

  @Test
  public void receiptOnThirdShoppingBasket() throws Exception {
    salesTaxesCalculator.fetchKnowledgeOfTaxFreeGoods("tax_free_items.txt");
    salesTaxesCalculator.fetchShoppingBasket("input/third_shopping_basket.txt");
    String expectedReceipt =
        getExpectedReceipt("output/expected_third_shopping_basket_receipt.txt");
    assertThat(salesTaxesCalculator.getReceipt()).isEqualTo(expectedReceipt);
  }

  private String getExpectedReceipt(String filename) throws Exception {
    URL expectedReceiptResource = getClass().getClassLoader().getResource(filename);
    Path expectedReceiptPath = Paths.get(Objects.requireNonNull(expectedReceiptResource).toURI());
    Stream<String> expectedReceiptLines = Files.lines(expectedReceiptPath);
    String expectedReceipt = expectedReceiptLines.collect(Collectors.joining("\n"));
    expectedReceiptLines.close();
    return expectedReceipt;
  }
}
