package sales.taxes.problem.item;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class ItemTest {

  @Test
  public void priceAndSalesTaxOnUntaxedItem() {
    Item item = new Item("2 book at 12.49");
    assertThat(item.getTotalPrice()).isEqualTo("24.98");
    assertThat(item.getSalesTax()).isEqualTo("0.00");
  }

  @Test
  public void priceAndSalesTaxOfTaxedItem() {
    Item item = new Item("1 bottle of perfume at 47.50");
    item.applyBasicSaleTax();
    assertThat(item.getTotalPrice()).isEqualTo("52.25");
    assertThat(item.getSalesTax()).isEqualTo("4.75");
  }

  @Test
  public void priceAndSalesTaxOfImportedItem() {
    Item item = new Item("1 imported bottle of perfume at 47.50");
    item.applyBasicSaleTax();
    item.applyImportDuty();
    assertThat(item.getTotalPrice()).isEqualTo("54.65");
    assertThat(item.getSalesTax()).isEqualTo("7.15");
  }

  @Test
  public void printItemResume() {
    Item item = new Item("1 bottle of perfume at 47.50");
    assertThat(item.getResume()).isEqualTo("1 bottle of perfume: 47.50");
  }

  @Test
  public void printImportedItemResume() {
    Item item = new Item("1 imported bottle of perfume at 47.50");
    assertThat(item.getResume()).isEqualTo("1 imported bottle of perfume: 47.50");
  }

  @Test
  public void printImportedItemResumeWithImportedSwapped() {
    Item item = new Item("1 bottle of imported perfume at 47.50");
    assertThat(item.getResume()).isEqualTo("1 imported bottle of perfume: 47.50");
  }
}
