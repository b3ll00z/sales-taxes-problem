This repo contains my implementation of **Sales taxes problem**. 
A full description of this exercise can be found [here](https://gitlab.com/b3ll00z/sales-taxes-problem/-/blob/master/SALES_TAXES_PROBLEM.md).


### Prerequisites

All sources are written in Java using Oracle JVM version 8.


### Installing

To install this project on your local machine just clone the repository as the prompt below reports.

```
$ git clone https://gitlab.com/b3ll00z/sales-taxes-problem.git
$ cd sales-taxes-problem/
```

### Build
```
$ ./gradlew assemble
```

### Running the tests

```
$ ./gradlew check
```

### Run the application
```
$ ./gradlew run
```


### Assumptions
A class ```SalesTaxesCalculator``` is responsible to evaluate the receipt details of a shopping basket given as file. Currently this class handles this input file as a basic textual file. To recognize if a good requires a basic sales tax or not I assumed that a knowledge of untaxed goods I given prior to a shopping basket acquisition, based on books, food and medical products.

#### Coding style tests

I've decided to apply [Google Java Style](https://google.github.io/styleguide/javaguide.html) for code formatting using [Spotless](https://github.com/diffplug/spotless). To check the compliance with Google's formatting rules just run:
```
$ ./gradlew spotlessCheck
```



## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Effective Java, 3rd Edition. by Joshua Bloch
* Clean Code: A Handbook of Agile Software. by Robert C. Martin


